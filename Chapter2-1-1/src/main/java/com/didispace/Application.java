package com.didispace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author 程序猿DD
 * @version 1.0.0
 * @blog http://blog.didispace.com
 * 
 * 在Spring Boot中多环境配置文件名需要满足application-{profile}.properties的格式，其中{profile}对应你的环境标识
 * application-dev.properties：开发环境
 * application-test.properties：测试环境
 * application-prod.properties：生产环境
 * 
 * 至于哪个具体的配置文件会被加载，需要在application.properties文件中通过spring.profiles.active属性来设置，其值对应{profile}值。
 * 
 */
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication sa = new SpringApplication();
		//通过下面的设置屏蔽命令行访问属性功能
		sa.setAddCommandLineProperties(false);
		SpringApplication.run(Application.class, args);

	}

}
